package me.ihwan.e_surat

class ApiEndPoint {
    companion object {

        //url server
        private val SERVER = "http://192.168.43.238/esurat/"
        val CREATE = SERVER+"create.php"
        val READ = SERVER+"read.php"
        val DELETE = SERVER+"delete.php"
        val UPDATE = SERVER+"update.php"

    }
}