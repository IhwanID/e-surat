package me.ihwan.e_surat

import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject


class MainActivity : AppCompatActivity() {

    private var gender = ""
    private var surat = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        genderGroup.setOnCheckedChangeListener { radioGroup, i ->

            when(i){

                R.id.pria->{
                    gender = "Pria"
                }

                R.id.wanita->{
                    gender = "Wanita"
                }

            }

        }

        suratGroup.setOnCheckedChangeListener { radioGroup, i ->

            when(i){

                R.id.nikah->{
                    surat = "nikah"
                }

                R.id.pindah->{
                    surat = "pindah"
                }

                R.id.keterangan->{
                    surat = "keteragan"
                }

            }

        }

        kirim.setOnClickListener {
            kirim()
        }

        lihat.setOnClickListener {
            startActivity(Intent(this, SuratActivity::class.java))
        }
    }

    private fun kirim(){
        val loading = ProgressDialog(this)
        loading.setMessage("Menambahkan data...")
        loading.show()

        AndroidNetworking.post(ApiEndPoint.CREATE)
                .addBodyParameter("nik",nik.text.toString())
                .addBodyParameter("nama",nama.text.toString())
                .addBodyParameter("alamat",alamat.text.toString())
                .addBodyParameter("gender",gender)
                .addBodyParameter("surat", surat)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {

                    override fun onResponse(response: JSONObject?) {
                        loading.dismiss()
                        Toast.makeText(applicationContext,response?.getString("message"),Toast.LENGTH_SHORT).show()
                    }

                    override fun onError(anError: ANError?) {
                        loading.dismiss()
                        Log.d("ONERROR",anError?.errorDetail?.toString())
                        Toast.makeText(applicationContext,"Connection Failure",Toast.LENGTH_SHORT).show()
                    }


                })
    }
}
