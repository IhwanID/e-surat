package me.ihwan.e_surat

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_surat.view.*

class RVAdapter(private val context: Context, private val arrayList: ArrayList<Surat>)
    : RecyclerView.Adapter<RVAdapter.Holder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(LayoutInflater.from(parent.context).inflate(R.layout.item_surat,parent,false))
    }

    override fun getItemCount(): Int {
        return arrayList!!.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.view.nik.text = "Nik : "+arrayList?.get(position)?.nik
        holder.view.nama.text = "Nama : "+arrayList?.get(position)?.nama
        holder.view.alamat.text = "Alamat : "+arrayList?.get(position)?.alamat
        holder.view.gender.text = "Gender: "+arrayList?.get(position)?.gender
        holder.view.surat.text = "Surat : "+arrayList?.get(position)?.surat
    }

    class Holder(val view: View) : RecyclerView.ViewHolder(view)
}