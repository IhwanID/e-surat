package me.ihwan.e_surat

import android.app.ProgressDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import kotlinx.android.synthetic.main.activity_surat.*
import org.json.JSONObject

class SuratActivity : AppCompatActivity() {

    var arrayList = ArrayList<Surat>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_surat)

        recyclerview.setHasFixedSize(true)
        recyclerview.layoutManager = LinearLayoutManager(this)

        loadData()
    }

    private fun loadData(){
        val loading = ProgressDialog(this)
        loading.setMessage("Load Surat dulu bro")
        loading.show()

        AndroidNetworking.get(ApiEndPoint.READ)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject?) {
                        arrayList.clear()
                        val jsonArray = response?.optJSONArray("result")

                        if(jsonArray?.length() == 0){
                            loading.dismiss()
                            Toast.makeText(applicationContext,"Surat is empty, Add the data first",Toast.LENGTH_SHORT).show()
                        }

                        for(i in 0 until jsonArray?.length()!!){

                            val jsonObject = jsonArray?.optJSONObject(i)
                            arrayList.add(Surat(jsonObject.getString("nik"),
                                    jsonObject.getString("nama"),
                                    jsonObject.getString("alamat"),
                                    jsonObject.getString("gender"),
                                    jsonObject.getString("surat")
                            ))

                            if(jsonArray?.length() - 1 == i){

                                loading.dismiss()
                                val adapter = RVAdapter(applicationContext,arrayList)
                                adapter.notifyDataSetChanged()
                                recyclerview.adapter = adapter

                            }

                        }

                    }

                    override fun onError(anError: ANError?) {
                        loading.dismiss()
                        Log.d("ONERROR",anError?.errorDetail?.toString())
                        Toast.makeText(applicationContext,"Connection Failure",Toast.LENGTH_SHORT).show()
                    }

                })
    }
}
